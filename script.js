var OTHER_THRESHOLD = 1;
var list = [];
var totalSum = 0;
var listSmallerPerc = 0;
var listSmallerValue = 0;

/*Sum of world population*/

totalSum = data.reduce(function (prevItem, curItem) {
      return prevItem + curItem.population;
},0);

console.log(totalSum);

/*New massive list*/
list = data.map(function(item) {
    return {
        description: item.country,
        percentage: item.population / totalSum * 100,
        value: item.population
    }
});

/*Sorting of list massive*/
list.sort(function (a, b) {
    return b.percentage - a.percentage;
});

/*Smallest part of massive*/
function smallestPart() {
    var listSmaller = list.filter(function (item) {
        return item.percentage < OTHER_THRESHOLD;
    });

    if(listSmaller.length >= 2){
        listSmaller.forEach(function (item) {
            listSmallerPerc += item.percentage;
            listSmallerValue += item.value;
        });
        /*Rewriting of list massive with the biggest population*/
        list = list.filter(function (item) {
            return item.percentage > OTHER_THRESHOLD;
        });
        list.push({
            description: "Other",
            percentage: listSmallerPerc,
            value: listSmallerValue
        });
    }
}
smallestPart();

// visualisation
var canvas = document.getElementById('canvas');
canvas.width = 750;
canvas.height = 500;

var ctx = canvas.getContext('2d');

var startAngle = 0, colors = CSS_COLOR_NAMES.slice(0);
list.forEach(function (item, index, list) {
    // sector
    ctx.beginPath();
    ctx.fillStyle = colors.splice(Math.round(Math.random() * (colors.length - 1)), 1)[0];
    ctx.moveTo(250, 250);
    ctx.arc(250, 250, 200, startAngle, startAngle -= item.percentage * Math.PI / 50, true);
    ctx.lineTo(250, 250);
    ctx.fill();

    // legend
    var lHeight = 500 / list.length;
    ctx.fillRect(500, lHeight * index + (lHeight - 15) / 2, 15, 15);
    ctx.fillStyle = '#000';
    ctx.fillText(item.description + ' (' + item.percentage.toFixed(2) + '%)', 520, lHeight * index + (lHeight - 15) / 2 + 10);
});